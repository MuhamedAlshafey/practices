# OOP Practices #

### What is this repository for? ###

* This repository contains e a pratical examples which describes how to write good OOP code.

### Contribution guidelines ###

* You can find every exercise inside its folder. 
* Write what is good and bad.
* Give your suggestion.
* Rewrite each exercise to be good OOP code.
* When rewriting the exercise postfix your namespaces with your name eg. Shafey\Models, Sherief\Models.
* Create new branch for your new code like exercise-name/my-name eg. exercise001/shafey.
* Inside every branch create new readme.md file describing your opinion.

### Who do I talk to? ###

* You can ask directly to the post related to the exercise in Workplace.