<?php
namespace Models;

abstract class BaseModel
{

    protected $data;

    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    public function __get($name)
    {
        return isset($this->data[$name]) ? $this->data[$name] : NULL;
    }

    abstract protected function getFields();

    abstract protected function getModelTableName();

    public function insert()
    {
        $conn = $this->getConnection();
        $table = $this->getModelTableName();
        $fields = $this->getFields();
        $statement = $conn->prepare("INSERT INTO $table (" . implode(",", $fields) . ") VALUES (:" . implode(",:", $fields) . ");");
        foreach ($fields as $field) {
            $values[$field] = $this->$field;
        }
        $statement->execute($values);
        $this->id=$conn->lastInsertId();
        $this->cache();
    }
    
    public function update()
    {
        $conn = $this->getConnection();
        $table = $this->getModelTableName();
        $fields = $this->getFields();
        
        foreach ($fields as $field) {
            $updateFields[]=" $field=:$field ";
            $values[$field] = $this->$field;
        }
        $statement = $conn->prepare("UPDATE  $table SET  " . implode(",", $updateFields) . " WHERE id=:id ;");
        $statement->execute($values);
        $this->cache();
    }

    public function cache()
    {
        $filename = md5($this->id);
        file_put_contents('cache/' . $this->getModelTableName() . '/' . $filename . '.cache', serialize($this));
    }

    public function findById($id)
    {
        $filename = md5($id);
        if (file_exists('cache/' . $this->getModelTableName() . '/' . $filename . '.cache')) {
            return unserialize(file_get_contents('cache/' . $this->getModelTableName() . '/' . $filename . '.cache'));
        }
        $conn = $this->getConnection();
        $table = $this->getModelTableName();
        $statement = $conn->prepare(" SELECT * FROM $table WHERE id = :id;");
        $statement->execute(['id' => $id]);
        $result = $statement->fetch(\PDO::FETCH_ASSOC);
        if (!$result) {
            return NULL;
        }
        $class = __CLASS__;
        $model = new $class();
        foreach ($result as $key => $value) {
            $model->$key = $value;
        }
        $model->cache();
        return $model;
    }

    /**
     * 
     * @return \PDO
     */
    protected function getConnection()
    {
        try {
            $servername = "localhost";
            $username = "username";
            $password = "password";
            $conn = new \PDO("mysql:host=$servername;dbname=myDB", $username, $password);
            // set the PDO error mode to exception
            $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            return $conn;
        } catch (PDOException $e) {
            die("Connection failed: " . $e->getMessage());
        }
    }
}
