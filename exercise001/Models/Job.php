<?php
namespace Models;

/**
 * Description of JobModel
 *
 * @author shafey
 */
class Job extends BaseModel
{

    protected function getFields()
    {
        return [
            'id',
            'job_title',
            'description',
            'salary'
        ];
    }

    protected function getModelTableName()
    {
        return 'job';
    }

    public function addJob($data)
    {
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }

        $this->insert();

        $action=new JobAction(['action_date' => date("Y-m-d H:i:s"), 'job_id' => $this->id]);
        $action->insert();
    }

    public function updateJob($data)
    {
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
        $this->update();

        $action=new JobAction(['action_date' => date("Y-m-d H:i:s"), 'job_id' => $this->id]);
        $action->insert();
    }
}
