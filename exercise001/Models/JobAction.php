<?php

namespace Models;

/**
 * This is model lo log all actions taken with a job
 *
 * @author shafey
 */
class JobAction extends BaseModel
{
    public function __construct($data)
    {
        $this->data=$data;
    }
    
    protected function getFields()
    {
        return [
            'id',
            'job_id',
            'action_date'
        ];
    }
    
    protected function getModelTableName()
    {
        return 'job_action';
    }
    
    public function __set($name, $value)
    {
        throw new Exception('Trying to edit immutable object!');
    }
    
    public function update()
    {
        throw new Exception('Trying to edit immutable object!');
    }
}
